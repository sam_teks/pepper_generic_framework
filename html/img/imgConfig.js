var BGImg = {
  "b1": "cover.jpg",
  "b1b1": "main.jpg",

  "b1b1b1": "membership1.jpg",
  "b1b1b2": "membership2.jpg",
  "b1b1b3": "membership3.jpg",
  "b1b1b4": "membership4.jpg",
  "b1b1b5": "iamnew.jpg",

  "b1b1b1b1": "offer1.jpg",
  "b1b1b1b2": "offer2.jpg",
  "b1b1b1b3": "offer3.jpg",
  "b1b1b1b4": "offer4.jpg",
  "b1b1b1b5": "offer5.jpg",
  "b1b1b1b6": "offer6.jpg",

  "b1b1b2b1": "membership1.jpg",
  "b1b1b2b2": "membership1.jpg",
  "b1b1b2b3": "membership1.jpg",
  "b1b1b2b4": "membership1.jpg",
  "b1b1b2b5": "membership1.jpg",
  "b1b1b2b6": "membership1.jpg",

  "b1b1b3b1": "membership1.jpg",
  "b1b1b3b2": "membership1.jpg",
  "b1b1b3b3": "membership1.jpg",
  "b1b1b3b4": "membership1.jpg",
  "b1b1b3b5": "membership1.jpg",
  "b1b1b3b6": "membership1.jpg",

  "b1b1b4b1": "dance.jpg",
  "b1b1b4b2": "pose.jpg",
  "b1b1b4b3": "rpsgame.jpg",

  "p1": "p1.png",
}

var btnImg = {
  "up": "up.jpg",
  "down": "down.jpg",
  "home": "home.jpg",

  "b1b1d1": "membership.jpg",
  "b1b1d2": "latestpromotion.jpg",
  "b1b1d3": "lastestevent.jpg",
  "b1b1d4": "game.jpg",
  "b1b1d5": "iamnewbtn.jpg",

  "b1b1b1d1": "offer1.jpg",
  "b1b1b1d2": "offer2.jpg",
  "b1b1b1d3": "offer3.jpg",
  "b1b1b1d4": "offer4.jpg",
  "b1b1b1d5": "offer5.jpg",
  "b1b1b1d6": "offer6.jpg",

  "b1b1b2d1": "membership.jpg",
  "b1b1b2d2": "membership.jpg",
  "b1b1b2d3": "membership.jpg",
  "b1b1b2d4": "membership.jpg",
  "b1b1b2d5": "membership.jpg",
  "b1b1b2d6": "membership.jpg",

  "b1b1b3d1": "membership.jpg",
  "b1b1b3d2": "membership.jpg",
  "b1b1b3d3": "membership.jpg",
  "b1b1b3d4": "membership.jpg",
  "b1b1b3d5": "membership.jpg",
  "b1b1b3d6": "membership.jpg",

  "b1b1b4d1": "dance.jpg",
  "b1b1b4d2": "pose.jpg",
  "b1b1b4d3": "rpsgame.jpg",

}
