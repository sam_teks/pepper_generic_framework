var isDocReady = false;
var isOptionOnCD = false;
var isOptionReady = true;
var app = angular.module("myApp", []);
var N = 10;
var numberList = Array.apply(null, {length: N}).map(Number.call, Number);

$('button').click(function(){
    var btn = $(this);
    btn.prop('disabled', true);
    setTimeout(function(){
        btn.prop('disabled', false);
    },500);
});

app.controller("myCtrl", function($scope, $http) {
    $scope.isReocrding = true;
    $scope.isTouchDisable = false;

    $scope.videoID = "";

    $(".play-video").prop('muted', true);
    $('.play-video').click(function() {
        console.log("toggling");
        if ($(this).hasClass('play')) {
            $(this).trigger("pause");
            $(this).removeClass('play');
            $(this).addClass('pause');
        } else {
            $(this).trigger("play");
            $(this).removeClass('pause');
            $(this).addClass('play');
        }
   })

    $scope.clickToPlayVideo = function(pos){
        $scope.playVideo(pos);
        console.log("playing");
        session.raiseEvent("Teks/HKJockeyClub/html_video", "play");
    }

    $scope.playVideo = function(pos){
        console.log(pos);
        pos = typeof(pos) !== "undefined" ? pos :"b1";
        $scope.videoID = "video_" + pos ;
        
        console.log($scope.videoID);

        var $video = $("#"+ $scope.videoID);
        $video.prop('currentTime', 0);
        loadedVideo = true;
        $video.prop('muted', false);
        $video.trigger("play");
        $video.removeClass('pause');
        $video.addClass('play');
        $video.css('z-index',105);
        $(".misc-close-btn").css("z-index", 105);
        $(".misc-bg").css("z-index", 100);
        $video.css('opacity', 1);
    }

    $scope.clickToStopVideo = function(){
        $scope.stopVideo();
        console.log("stopping");
        session.raiseEvent("Teks/HKJockeyClub/html_video", "stop");
    }

    $scope.stopVideo = function(){
        console.log("stop video");
        $(".misc-close-btn").css("z-index", 0);
        $(".misc-bg").css("z-index", 0);
        $(".play-video").prop("currentTime",0);
        $(".play-video").css("z-index", 1);
        $(".play-video").css("opacity", 0);
        $(".play-video").trigger("pause");
	    $(".play-video").removeClass('play');
	    $(".play-video").addClass('pause');
    }

    var itemsFromCSV = [];

    function parseCSV($http) {
        console.warn("calling parse CSV");
        $http.get("ProductList.csv").success(function(response) {
            console.warn(response);
            var itemLine = response.split(/\r\n/);
            console.log(itemLine);
            itemLine.shift();
            itemLine.shift();
            for (i = 0; i < itemLine.length; i++) {
                var itemAttrs = itemLine[i].split(/,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/);
                itemsFromCSV.push({
                    Item: parseInt(itemAttrs[0]) - 1,
                    Brand: itemAttrs[1].replace(/"/g, ''),
                    Model: itemAttrs[2].replace(/"/g, ''),
                    Description: itemAttrs[3].replace(/"/g, ''),
                    Highlight1: itemAttrs[4].replace(/"/g, ''),
                    Highlight2: itemAttrs[5].replace(/"/g, ''),
                    Highlight3: itemAttrs[6].replace(/"/g, ''),
                    Thumbnails: $scope.getThumbnailSrc(itemAttrs[7]),
                    Photo: $scope.getPhotoSrc(itemAttrs[8]),
                    Month: itemAttrs[9].replace(/"/g, ''),
                    SRP: itemAttrs[10].replace(/"/g, ''),
                    SPFM: itemAttrs[11].replace(/"/g, ''),
                    Remark: itemAttrs[12].replace(/"/g, ''),
                    Category: itemAttrs[13].replace(/"/g, '').replace(/[\n\r]/g, '').replace(/ /g, ''),
                    Points: itemAttrs[17].replace(/"/g, ''),
                    PointRoundUp : itemAttrs[18].replace(/"/g, ''),
                    SP_Redem_Points : itemAttrs[19].replace(/"/g, ''),
                    ProdCategory :itemAttrs[20].replace(/"/g, ''),
                    itemCode :itemAttrs[21].replace(/"/g, ''),
                    desp_cn :itemAttrs[22].replace(/"/g, ''),
                    desp_en :itemAttrs[23].replace(/"/g, ''),

                });
            }
            var testarray = {};
            console.warn("success");
            console.warn(itemsFromCSV);
            console.warn(itemsFromCSV[0]);
        });
    }
    parseCSV($http);
    var specialPageData = {
        "list": itemsFromCSV
    };

    $scope.getItemsFromCSV = function(item) {
        return itemsFromCSV[item];
    }

    $scope.mapImgSrc = function(img) {
        var imgSrc = "img/main/" + BGImg[img];
        console.log(imgSrc);
        return imgSrc
    }
    $scope.mapBtnImg = function(img) {
        var imgSrc = "img/main/" + btnImg[img];
        console.log(imgSrc);
        return imgSrc
    }
    $scope.getBGImage = function(key) {
        var myObj;
        if (key == 1) {
            myObj = {
                "background-image": "url('img/menu_option1.png')"
            }
        } else if (key == 2) {
            myObj = {
                "background-image": "url('img/menu_option2.png')"
            }
        }
        return myObj
    }
    $scope.getBtnImg = function(type, digit) {
        var curPos = $scope.pagePos;
        var btnImgPos;

        type = typeof type !== 'undefined' ? type : 'down';
        digit = typeof digit !== 'undefined' ? digit : '1';
        switch (type) {
            case "down-single":
                btnImgPos = curPos + "d" + digit;
                break;
            case "trigger-event":
                btnImgPos = curPos + "t" + digit;
                break;
            case "home":
                btnImgPos = "home";
                console.log(btnImgPos);
                break;
            case "down":
                btnImgPos = "down";
                console.log(btnImgPos);
                break;
            case "up":
                btnImgPos = "up";
                break;
        }
        var btnImgSrc = $scope.mapBtnImg(btnImgPos);
        // console.log(btnImgSrc);
        var myObj = {
            "background-image": "url('" + btnImgSrc + "')"
        }
        return myObj
    }
    $scope.showModal = false;
    $scope.toggleModal = function() {
        $scope.goPopUp();
        $scope.showModal = !$scope.showModal;
    };
    $scope.showFSModal = false;
    $scope.fSButtonClicked = "";
    $scope.toggleFSModal = function(btnClicked) {
        $scope.fSButtonClicked = btnClicked;
        $scope.showFSModal = !$scope.showFSModal;
    };
    $scope.isDisplay = false;
    $scope.appLayout = "option-bullet-3";
    // $scope.appLayout = "option-2x2-grid";
    $scope.pagePos = "b1";
    $scope.imgSrc = $scope.mapImgSrc($scope.pagePos);
    $scope.isGif = false;
    $scope.gif_flow = 11;
    $scope.optionList = ["係", "唔係"];
    $scope.numberList = numberList;
    $scope.popUpImgSrc = $scope.mapImgSrc($scope.pagePos + "p1");

    $scope.webStartRecord = function() {
        $scope.$apply(function() {
            $scope.isReocrding = true;
        })
    }

    $scope.webStopRecord = function() {
        $scope.$apply(function() {
            $scope.isReocrding = false;
        })
    }

    $scope.webDisableTouch = function() {
        $scope.$apply(function() {
            $scope.isTouchDisable = true;
        })
    }

    $scope.webEnableTouch = function() {
        $scope.$apply(function() {
            $scope.isTouchDisable = false;
        })
    }

    $scope.goDownClass = function(index) {
        return "go-down-" + index;
    }

    $scope.goTriggerClass = function(index){
        return "go-trigger-" + index;
    }

    $scope.goOneToManyDownClass = function(index) {
        return "go-one-to-many-down-" + index;
    }

    $scope.goSiblingClass = function(index) {
        return "go-sibling-" + index;
    }

    //$scope.goHome = function() {
        //var curPos = $scope.pagePos;
        //$scope.pagePos = "b1";
        //$scope.imgSrc = $scope.mapImgSrc($scope.pagePos);
        //console.log(curPos + "goto" + "b1");
    //}

    $scope.goToSpecPos = function(position) {
        var curPos = $scope.pagePos;
        $scope.pagePos = position;
        $scope.imgSrc = $scope.mapImgSrc($scope.pagePos);
        $scope.loadPageImage(position);
    }

    $scope.goPopUp = function(digit) {
        digit = typeof digit !== 'undefined' ? digit : '1';
        var curPos = $scope.pagePos;
        var popUpImgPos = curPos + "p" + digit;
        $scope.popUpImgSrc = $scope.mapImgSrc(popUpImgPos);
        console.log("pop" + popUpImgPos);
    }

    $scope.goToSpecPopUp = function(specImgPos, fullscreen) {
        var curPos = $scope.pagePos;
        specImgPos = typeof specImgPos !== 'undefined' ? specImgPos : 'p1';
        fullscreen = typeof fullscreen !== 'undefined' ? fullscreen : 'false';
        $scope.popUpImgSrc = $scope.mapImgSrc(specImgPos);

        if (fullscreen == "false") {
            $scope.showModal = true;
        } else if (fullscreen == "true") {
            $scope.showFSModal = true;
        }

        console.log("popping" + specImgPos);
        $scope.$apply();

    }

    $scope.cancelSpecPopUp = function() {
        var curPos = $scope.pagePos;
        $scope.showModal = false;
        $scope.showFSModal = false;
        $scope.isDisplay = false;
        isOptionReady = true;
        $scope.$apply();
        session.raiseEvent("Teks/HKJockeyClub/html", "popCancel" + curPos);
        console.log("popCancel");
    }

    $scope.popUpQnA = function(specImgPos) {
        specImgPos = typeof specImgPos !== 'undefined' ? specImgPos : 'p1';
        $scope.popUpImgSrc = $scope.mapImgSrc(specImgPos);
        $scope.showFSModal = true;
        $scope.isDisplay = true;
        $scope.$apply();
    }

    $scope.goTrigger= function(index){
        index = typeof index !== 'undefined' ? index : 1;
        console.log("trigger index",index);
        var text = "";
        switch(index) {
            case 1:
                text = "流動應用程式a1";
                break;
            case 2:
                text = "流動應用程式a2";
                break;
            case 3:
                text = "流動應用程式a3";
                break;
            case 4:
                text = "流動應用程式a4";
                break;
            case 5:
                text = "流動應用程式a5";
                break;
            case 6:
                text = "流動應用程式a6";
                break;
            break;
        }
        console.log("text",text);
        session.raiseEvent("teks/tp_ct_c_newworldproperty/html", text);
        $scope.$apply();
    }

    $scope.goToPos = function(direction, position) {
        direction = typeof direction !== 'undefined' ? direction : "down";
        position = typeof position !== 'undefined' ? position : '1';
        var digit;
        var newPos;
        var curPos = $scope.pagePos;
        direction = direction.toLowerCase();
        switch (direction) {
            case "home":
                var n = curPos.lastIndexOf("b");
                var imgbranch = "b1";
                //var imgbranch = curPos.substring(0, n);
                newPos = imgbranch;
                break;

            case "up":
                var n = curPos.lastIndexOf("b");
                var imgbranch = curPos.substring(0, n);
                newPos = imgbranch;
                break;

            case "one-to-many-up":
                var n;
                var x;
                var imgbranch;
                n = curPos.lastIndexOf("b");
                imgbranch = curPos.substring(0, n);
                x = imgbranch.lastIndexOf("b");
                imgbranch = imgbranch.substring(0, x);
                newPos = imgbranch;
                break;

            case "down":
                newPos = curPos + "b" + position;
                break;

            case "one-to-many-down":
                newPos = curPos + "b" + position + "b1";
                break;

            case "left":
                var n = curPos.lastIndexOf("b");
                var imgbranch = curPos.substring(0, n);
                digit = parseInt(curPos.substring(n + 1, curPos.length)) - 1;
                if (digit <= 0)
                    return;
                newPos = imgbranch + "b" + digit;
                break;

            case "right":
                var n = curPos.lastIndexOf("b");
                var imgbranch = curPos.substring(0, n);
                digit = parseInt(curPos.substring(n + 1, curPos.length)) + 1;
                newPos = imgbranch + "b" + digit;
                break;

            case "sibling":
                var n = curPos.lastIndexOf("b");
                var imgbranch = curPos.substring(0, n);
                digit = position;
                newPos = imgbranch + "b" + digit;
                break;
        }
        $scope.loadPageImage(newPos, curPos);
    }

    $scope.loadPageImage = function(newPos, curPos) {
        newPos = typeof newPos !== 'undefined' ? newPos : 'b1';
        curPos = typeof curPos !== 'undefined' ? curPos : '';
        var imgLoader = new Image();

        imgLoader.onload = function() {
            $scope.$apply(function() {
                $scope.pagePos = newPos;
                $scope.imgSrc = $scope.mapImgSrc(newPos);
                //session.raiseEvent("Teks/HKJockeyClub/SayArbText", curPos+"goto" + newPos);
                session.raiseEvent("Teks/HKJockeyClub/html_touch", curPos + "goto" + newPos);
                console.log(curPos + "goto" + newPos);
            });
        };
        imgLoader.onerror = function() {};
        imgLoader.src = $scope.mapImgSrc(newPos);
    }
    //options layout
    $scope.opt_show = function() {
        $scope.$apply(function() {
            $scope.isDisplay = true;
        })
        isOptionReady = true;
    }

    $scope.opt_hide = function() {
        $scope.$apply(function() {
            $scope.isDisplay = false;
        })
        isOptionReady = true;
    }

    $scope.opt_update = function(optionList) {
        if (optionList) {
            switch (optionList.length) {
                case 2:
                    $scope.appLayout = "option-bullet-2";
                    console.log("switched to bullet-2 layout");
                    break;
                case 3:
                    $scope.appLayout = "option-bullet-3";
                    console.log("switched to bullet-3 layout");
                    break;
                case 4:
                    $scope.appLayout = "option-2x2-grid";
                    console.log("switched to 2x2-grid layout");
                    break;
            }
            $scope.optionList = optionList;
        }
    }

    $scope.selectOption = function(selectedOption) {
        // console.log(selectedOption);
        // session.raiseEvent("Teks/HKJockeyClub/SayArbText", selectedOption);
        if (!isOptionOnCD) {
            // console.log("isOptionOnCD");
            if (isOptionReady) {
                isOptionReady = false;
                console.log(selectedOption);
                //session.raiseEvent("Teks/HKJockeyClub/SayArbText", selectedOption);
                session.raiseEvent("Teks/HKJockeyClub/html", selectedOption);
            }
            isOptionOnCD = true;
            setTimeout(function() {
                isOptionOnCD = false;
            }, 1000);
        }

    }

    $scope.test = function(param1) {
        console.log(param1);
    }
    $scope.test1 = function(param1) {

    }
    $scope.webPopOptions = function(option) {
        //eng
        var splitedOption = option.split(",");
        console.log(splitedOption);
        $scope.opt_update(splitedOption);
        $scope.opt_show();
        $scope.popUpQnA('p1');
    }

    session.subscribeToEvent("Teks/HKJockeyClub/CallFunction", function(data) {
        console.log("testing");
        if (data) {
            var splitedData = data.split("|");
            var func = splitedData[0];

            if (splitedData.length == 1) {
                eval("$scope." + func + "()");
            } else if (splitedData.length == 2) {
                var param1 = splitedData[1];
                eval("$scope." + func + "(" + param1 + ")");
            } else if (splitedData.length == 3) {
                var param2 = splitedData[2];
                eval("$scope." + func + "(" + param1 + ", " + param2 + ")");
            }
        }
    })

    session.subscribeToEvent("Teks/HKJockeyClub/ShowQuestionContent", function(data) {
        $scope.opt_hide();
        $scope.$apply(function() {
            $scope.isGif = false;
        });
    });
});

$(document).ready(function() {
    isDocReady = true;
    session.raiseEvent("Teks/HKJockeyClub/Test1", "1");
    setTimeout(function() {
        document.getElementById("DocReady").style.visibility = "visible";
    }, 100);
});
app.directive('modal', function() {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog pop-up-dialog">' +
            '<button type="button" class="pop-up-btn" data-dismiss="modal" aria-hidden="true">clsoe</button>' +
            '<div class="pop-up-img-body" ng-transclude></div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.$watch(attrs.visible, function(value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function() {
                scope.$apply(function() {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function() {
                scope.$apply(function() {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
app.directive('fsmodal', function() {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog full-screen-pop-up-dialog">' +
            '<div class="full-screen-pop-up-img-body" ng-transclude></div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.$watch(attrs.visible, function(value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function() {
                scope.$apply(function() {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function() {
                scope.$apply(function() {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
